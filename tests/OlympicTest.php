<?php
use PHPUnit\Framework\TestCase;

include 'Competition.php';

class OlympicTest extends TestCase
{
    private $competition;

    protected function setUp(): void
    {
        $this->competition = new Competition();
    }

    public function testBestOlympic()
    {
        $this->assertEquals('Barcelona', $this->competition->getBestOlympicByMembersCount()->getCountry());
    }

    /**
     * @dataProvider addDataProvider
     */
    public function testCountryByYear($year, $expected)
    {

        $this->assertEquals($expected, $this->competition->getCountryByYear($year));
    }

    public function addDataProvider()
    {
        return [
            [2000, 'London'],
            [2004, 'Sidney'],
            [2016, 'Barcelona'],
        ];
    }


    protected function tearDown(): void
    {
        unset($this->competition);
    }
}