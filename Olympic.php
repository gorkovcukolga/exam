<?php


class Olympic
{
    private $country;
    private $year;
    private $members_count;

    /**
     * Olympic constructor.
     * @param $country
     * @param $year
     * @param $members_count
     */
    public function __construct($country, $year, $members_count)
    {
        $this->country = $country;
        $this->year = $year;
        $this->members_count = $members_count;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getMembersCount()
    {
        return $this->members_count;
    }

    /**
     * @param mixed $members_count
     */
    public function setMembersCount($members_count)
    {
        $this->members_count = $members_count;
    }



}