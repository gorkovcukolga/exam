<?php

require 'Olympic.php';

class Competition
{
    private $olympics;

    public function __construct()
    {
        $this->olympics = [
            new Olympic('London', 2000, 100),
            new Olympic('Sidney', 2004, 67),
            new Olympic('Barcelona', 2016, 324),
        ];
    }

    public function getBestOlympicByMembersCount(){
        $best = $this->olympics[0];

        foreach ($this->olympics as $olympic){
            if ($olympic->getMembersCount() > $best->getMembersCount())
                $best = $olympic;
        }
        return $best;
    }

    public function getCountryByYear($year){
        foreach ($this->olympics as $olympic){
            if ($olympic->getYear() == $year)
                return $olympic->getCountry();
        }
        return null;
    }


}